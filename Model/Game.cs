namespace GameLibrary.Model;

public class Game {

    public int Id {get; set;}
    public string? Title { get; set;}
    public DateTime DatePurchased { get; set; }
    public bool Completed { get; set;}
}