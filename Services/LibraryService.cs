using GameLibrary.Model;

namespace GameLibrary.Service;

public static class LibraryService 
{
    static List<Game> Games { get; }
    static int currentId = 0;

    static LibraryService()
    {
        Games = new List<Game>();
    }

    // Get Methods
    public static List<Game> GetAll() => Games;

    public static Game? Get(int id) => Games.FirstOrDefault(g => g.Id == id);

    // create game
    public static void Add(Game g) {
        g.Id = currentId++;
        Games.Add(g);   
    }

    // update game
    public static void Update(Game g) {
        var index = Games.FindIndex(h => g.Id == h.Id);
        if (index == -1)
            return;

        Games[index] = g;
    }

    // delete game
    public static void Delete(int id) {
        var g = Get(id);
        if (g is null)
            return;

        Games.Remove(g);
    }

}