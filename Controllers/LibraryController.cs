using GameLibrary.Model;
using GameLibrary.Service;
using Microsoft.AspNetCore.Mvc;

namespace GameLibrary.Controllers;

[ApiController]
[Route("[controller]")]
public class LibraryController : ControllerBase
{
    public LibraryController() {}

    // TODO: How to use a Logger?
    // private readonly ILogger<LibraryController> _logger;
    // public LibraryController(ILogger<LibraryController> logger)
    // {
    //     _logger = logger;
    // }

    [HttpGet]
    public ActionResult<List<Game>> GetAll() => 
        LibraryService.GetAll();

    [HttpGet("{id}")]
    public ActionResult<Game> Get(int id) {
        
        var g = LibraryService.Get(id);

        if (g is null)
            return NotFound();

        return g;
    }


    [HttpPost]
    public IActionResult Create(Game game) {
        LibraryService.Add(game);
        return CreatedAtAction(nameof(Create), new { id = game.Id }, game);
    }

    [HttpPut("{id}")]
    public IActionResult Update(int id, Game game) {

        if (id != game.Id)
            return BadRequest();

        var g = LibraryService.Get(id);
        if (g is null)
            return NotFound();

        LibraryService.Update(game);

        return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(Game game) {
        var g = LibraryService.Get(game.Id);

        if (g is null)
            return NotFound();

        LibraryService.Delete(g.Id);

        return NoContent();
    }
}
